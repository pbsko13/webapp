(function($) {

	"use strict";

})(jQuery);


firebase.initializeApp({
    apiKey: "AIzaSyCfFdXPJ_rHxq4BN0M16m_67UuTSm6c2w0",
    authDomain: "tfgbaseapp.firebaseapp.com",
    projectId: "tfgbaseapp",
  });
  
  var db = firebase.firestore();
  

  var tabla = document.getElementById('tabla');
  var estado;
  var boton;
  db.collection("Cajas").onSnapshot((querySnapshot) => {
    tabla.innerHTML='';
    querySnapshot.forEach((doc) => {
		estado = doc.data().Ubicación;

    let unix_timestamp = doc.data().LLegada;
// Create a new JavaScript Date object based on the timestamp
// multiplied by 1000 so that the argument is in milliseconds, not seconds.
var date = new Date(unix_timestamp * 1000);
var dia = date.getDay() - 2;
var mes = 1 + date.getMonth();
var año = date.getFullYear() -1970  ;
// Hours part from the timestamp
var hours = date.getHours();
// Minutes part from the timestamp
var minutes = "0" + date.getMinutes();
// Seconds part from the timestamp
var seconds = "0" + date.getSeconds();

// Will display time in 10:30:23 format
var formattedTime =dia + "/" + mes + "/" + año + " a las " + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
		
	if (estado == "Casa"){
        console.log(`${doc.id} => ${doc.data()}`);
        tabla.innerHTML += `        <tr>
        <th scope="row">${doc.id}</th>
        <td>${doc.data().Medicamento}</td>
        <td>${doc.data().Cantidad}</td>
        <td>${formattedTime}</td>
		<td><a href="#" class="btn btn-success">Casa</a></td>
      </tr> `
	}
	else {

        console.log(`${doc.id} => ${doc.data()}`);
        tabla.innerHTML += `        <tr>
        <th scope="row">${doc.id}</th>
        <td>${doc.data().Nombre}</td>
        <td>${doc.data().Tipo}</td>
        <td>${doc.data().Cantidad}</td>
		<td><a href="#" class="btn btn-warning">No</a></td>
      </tr> `

	}
    });
});

